import React, { Component } from "react";
import SignUp from "./signup";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './win.css'
export default class Login extends Component {
    
    render() {
        
        return (
            <form action="blog-overview">
                <h3>Sign In</h3>
                
                <div className="form-group">
                    <label class>Email address</label>
                    <input type="email" className="form-control" placeholder="Enter email" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" />
                </div>

                {/* <div className="form-group">
                    <div className="custom-control custom-checkbox">
                        <input type="checkbox" className="custom-control-input" id="customCheck1" />
                        <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
                    </div>
                </div> */}

                <button type="submit" className="btn-xl btn-primary btn-block">Submit</button>
                <p className="forgot-password text-right">
                    New User ? <a href="/Signup">Register</a>
                </p>
             
            </form>
        );
    }
    
}

